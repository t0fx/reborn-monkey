import json 

fileName = "wifibrisbane"
data = open(fileName, "r+")
myFile = data.read()

#print myFile
jsonObj = json.loads(myFile)
i = 1
for e in jsonObj:
	e['productId'] = e['Wifi Hotspot Name'] + e['id'] 
	e['productName'] = e['Wifi Hotspot Name']
	e['productCategoryId'] = "WIFI"
	e['boundary'] = e['Latitude'] + "," + e['Longitude']
	e['distanceToLoctation'] = ""
	i +=1

text = json.dumps(jsonObj)
data.write(text)
data.close()
