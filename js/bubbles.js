function makeBubbles(root){
  var diameter = $(window).width(),
      diameterh = $(window).height() - $('header').height(),
      format = d3.format(",d"),
      color = d3.scale.category20();

  var bubble = d3.layout.pack()
      .sort(null)
      .size([diameter, diameterh])
      .value(function(d){return d.value; })
      .padding(1.5);

  var svg = d3.select("svg")
      .attr("width", diameter)
      .attr("height", diameterh)
      .attr("class", "bubble");

  var node = svg.selectAll(".node")
      .data(bubble.nodes(classes(root)).filter(function(d) { return !d.children; }), function(d){  console.log(d); return d.className; });


  node.append("title")
      .text(function(d) { return d.className + ": " + format(d.value); });

  node.append("circle")
      .style("fill", function(d) { return color(d.packageName); })
      .on("click", function(d) { window.location = d.url; })
      .attr("r", 0)
      .transition()
      .duration(1000)
      .attr("r", function(d) { return d.r; });

  node.transition().duration(1000).attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  node.exit().transition().duration(200).attr("transform", "scale(0.001)").remove();

  node.append("text")
      .attr("dy", ".3em")
      .style("text-anchor", "middle")
      .text(function(d) { return d.className.substring(0, d.r / 6); })
      .attr("opacity",0)
      .transition().duration(1000)
      .attr("opacity",1);

// Returns a flattened hierarchy containing all leaf nodes under the root.
  function classes(root) {
    var classes = [];

    function recurse(name, node) {
      if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
      else classes.push({packageName: name, className: node.name, value: node.size, url: node.url});
    }

    recurse(null, root);
    return {children: classes};
  }

  d3.select(self.frameElement).style("height", diameterh + "px");
}