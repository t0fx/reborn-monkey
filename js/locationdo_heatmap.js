function dostuff(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    var cat = (window.location.hash).substr(1);

    console.log(cat);
    if(!cat) { $('h2').html("No category.."); return; }

    $('#name').html(cat);

    //alert("( " + latitude + ", " + longitude + "); accuracy: " + position.coords.accuracy + "; heading: " + position.coords.heading);

    getTourismDataByCat(latitude, longitude, cat);

}