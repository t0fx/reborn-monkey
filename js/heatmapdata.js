function makeColour(n){
    
    var h = n * 0.4; // Hue (note 0.4 = Green
    var s = 0.9; // Saturation
    var b = 0.9; // Brightness

    var rgb = hsvToRgb(h,s,b);
    var hex = rgbToHex("rgb("+rgb['red']+", "+rgb['green']+", "+rgb['blue']+")");

    return hex;

}

function rgbToHex(rgb) {
    if (rgb.substr(0, 1) === '#') {
        return rgb;
    }
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(rgb);
    
    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);
    
    var rgb = blue | (green << 8) | (red << 16);
    return digits[1] + '#' + rgb.toString(16);
}

function hsvToRgb(h,s,v) {
    // Adapted from http://www.easyrgb.com/math.html
    // hsv values = 0 - 1, rgb values = 0 - 255
    var r, g, b;
    var RGB = new Array();
    if(s==0){
      RGB['red']=RGB['green']=RGB['blue']=Math.round(v*255);
    }else{
      // h must be < 1
      var var_h = h * 6;
      if (var_h==6) var_h = 0;
      //Or ... var_i = floor( var_h )
      var var_i = Math.floor( var_h );
      var var_1 = v*(1-s);
      var var_2 = v*(1-s*(var_h-var_i));
      var var_3 = v*(1-s*(1-(var_h-var_i)));
      if(var_i==0){ 
        var_r = v; 
        var_g = var_3; 
        var_b = var_1;
      }else if(var_i==1){ 
        var_r = var_2;
        var_g = v;
        var_b = var_1;
      }else if(var_i==2){
        var_r = var_1;
        var_g = v;
        var_b = var_3
      }else if(var_i==3){
        var_r = var_1;
        var_g = var_2;
        var_b = v;
      }else if (var_i==4){
        var_r = var_3;
        var_g = var_1;
        var_b = v;
      }else{ 
        var_r = v;
        var_g = var_1;
        var_b = var_2
      }
      //rgb results = 0 ÷ 255  
      RGB['red']=Math.round(var_r * 255);
      RGB['green']=Math.round(var_g * 255);
      RGB['blue']=Math.round(var_b * 255);
      }
    return RGB;  
};

function getTourismDataByCat(latitude, longitude, cat){

    // Fix categories from slugs
    cat = cat.toUpperCase();
    if(cat == "ACCOMMODATION") { cat = "ACCOMM"; }
    else if(cat == "TOURISTDESTINATION") { cat = "DESTINFO"; }
    else if(cat == "TOURISTINFORMATION") { cat = "INFO"; }

    var max = 200; // 200 is arbitrary max

    var url = "http://govhack.atdw.com.au/productsearchservice.svc/products?key=278965474541&latlong=" + latitude + "," + longitude + "&dist=10&cats=" + cat + "&size=" + max + "&out=json";

    $.getJSON(url, function(tourism_data) { // Get raw JSON from tourism API

        var total = tourism_data.numberOfResults;
        var results = tourism_data.products;

        var n = total / max;
        n = 1 - n;

        var colour = makeColour(n);
        console.log(colour);
        //console.log(results);

        $('#heatmap').css("background-color",colour);
        $('#size').html(total);

    });

}