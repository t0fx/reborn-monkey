Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
}

function getTourismData(latitude, longitude){
   var url = "http://govhack.atdw.com.au/productsearchservice.svc/products?key=278965474541&latlong=" + latitude + "," + longitude + "&dist=10&size=200&out=json";

    $.getJSON("./data/base.json", function(data) { // Get base JSON for bubbles

        $.getJSON(url, function(tourism_data) { // Get raw JSON from tourism API

            results = tourism_data.products;

            // Sort/filter/rearrange tourism data
            var cats = getCategoriesAndSizes(results);
            var tourism_places = sortTourismCategories(cats);
            var biggest = getBiggestCategory(cats);
            // Slot tourism data into bubbles base
            tourism_json = makeJsonFromBase(data, tourism_places); 
            console.log("HERE:" + JSON.stringify(tourism_json));
            // Make bubbles
            makeBubbles(tourism_json);
            // Update 'what is this area like'
            makeAreaSummary(biggest);

        });

    });
}

function getCategoriesAndSizes(data){
    var track = [];
    var cats = [];
    $.each(data, function(key, val){

        if(!track.contains(val.productCategoryId)){ // do this less retardedly sometime
            track.push(val.productCategoryId);
            // fix stupid category names
            if(val.productCategoryId == "ACCOMM") { val.productCategoryId = "ACCOMMODATION"; }
            else if(val.productCategoryId == "DESTINFO") { val.productCategoryId = "TOURIST DESTINATION"; }
            else if(val.productCategoryId == "INFO") { val.productCategoryId = "TOURIST INFORMATION"; }
            var slug = val.productCategoryId.replace(/\s+/g, '').toLowerCase();
            cats.push({"name": val.productCategoryId, "size": 1, "url":"heatmap.html#"+slug});
        }else{
            for(var i=0; i < cats.length; i++){
                if(cats[i].name == val.productCategoryId){
                    cats[i].size = cats[i].size +1;
                }
            }
        }
    });
    return cats;
}

function getBiggestCategory(cats){

    var max = 0;
    var maxname;
    $.each(cats, function(key,val){
        if(val.size > max){ max = val.size; maxname = val.name; }
    });

    var biggest = [];
    biggest.push(max);
    biggest.push(maxname);

    return biggest;

}

function sortTourismCategories(cats){

    var practical = [], activity = [], leisure = [];

    for(var i=0; i < cats.length; i++){
        if(cats[i].name == "ACCOMM" || cats[i].name == "DESTINFO" || cats[i].name == "HIRE" || cats[i].name == "INFO" || cats[i].name == "TRANSPORT") { practical.push(cats[i]); }
        else if(cats[i].name == "ATTRACTION" || cats[i].name == "RESTAURANT") { leisure.push(cats[i]); }
        else if(cats[i].name == "EVENT" || cats[i].name == "JOURNEY" || cats[i].name == "TOUR") { activity.push(cats[i]); }
    }

    var tourism = {"name":"tourism", "children":[{"name":"practical","children":practical},{"name":"activity","children":activity},{"name":"leisure","children":leisure}]};

    return tourism;
}

function makeJsonFromBase(basedata, sorted_children){

    var top = new Object();

    top.name = "tourism";
    top.children = [sorted_children];

    if(basedata.name == "bubbles"){
        
        basedata.children.push(top);

    }

    return basedata;
}

function makeAreaSummary(biggest){
    var msg;
    if(biggest[0] == 0){ msg = "There doesn't seem to be much here."; }
    else { msg = "This is a <span class='up'>" + biggest[1] + "</span> hotspot!"; }
    $('#summary').html(msg);
}

function debug_printList(data){

    var items = [];

    $.each(data, function(key, val) {
        items.push("<li>" + key + ": " + JSON.stringify(val) + "</li>");
    });

    $('body').prepend(items);

}

function debug_printUrl(url){
    $('body').prepend(url);
}

function debug_printLen(obj){
    $('body').prepend(Object.keys(obj).length);
}