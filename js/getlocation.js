jQuery(window).ready(function(){ 

    //setInterval(fakeUpdate, 1000);

	var watchProcess = null; 

	if (watchProcess == null) {  
	    watchProcess = navigator.geolocation.watchPosition(dostuff, handle_errors, {enableHighAccuracy: true});  
	}
});

function fakeUpdate(){
    var lat = getRandomInRange(-180, 180, 3);
    var lon = getRandomInRange(-180, 180, 3);

    //getTourismData(lat, lon);
}

function getRandomInRange(from, to, fixed) {
    return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
    // .toFixed() returns string, so ' * 1' is a trick to convert to number
}

function handle_errors(error)  
{  
    switch(error.code)  
    {  
        case error.PERMISSION_DENIED: alert("You don't seem to have allowed us access to your location.");  
        break;  

        case error.POSITION_UNAVAILABLE: alert("We could not detect your location.");  
        break;  

        case error.TIMEOUT: alert("We could not detect your location (the request timed out).");  
        break;  

        default: alert("Something went wrong and we could not detect your location.");  
        break;  
    }  
}  

function stop_watchlocation() {  
    if (watchProcess != null)  
    {  
        navigator.geolocation.clearWatch(watchProcess);  
        watchProcess = null;  
    }  
}  
